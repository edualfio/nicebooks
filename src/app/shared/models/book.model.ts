export class Book {
	constructor(
		public id: string,
		public title: string,
		public author: string,
		public imageUrl: string,
		public genres: Array<string>,
		public description: string
	) { }
}
