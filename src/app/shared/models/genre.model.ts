export class Genre {
	constructor(
		public id: string,
		public code: string,
		public name: string,
		public description: string
	) { }
}
