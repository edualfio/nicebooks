export const books = [
	{
		id: '5c703578f93a62cdcd3273bd',
		title: 'El adversario',
		author: 'Emanuelle Carrère',
		imageUrl: 'https://www.anagrama-ed.es/uploads/media/portadas/0001/22/fd562616f54f77763acaac0bf1bdf65a7676d41f.jpeg',
		genres: [
			'drama',
			'biography'
		],
		description: 'Anim laboris qui occaecat aute nostrud culpa esse labore quis et exercitation excepteur quis ins.'
	},
	{
		id: '5c70357858063cb8fc53d0b7',
		title: 'Menos que cero',
		author: 'Brett Easton Ellis',
		imageUrl: 'http://184.172.120.194/~masterli/admin/files/libros/8395/9788433920560.jpg',
		genres: [
			'drama'
		],
		description: 'Aliquip excepteur anim veniam ullamco minim sit magna nostrud ex. Id dolor incididunt duis in amet.'
	},
	{
		id: '5c703578954b5686b11643a9',
		title: 'La tregua',
		author: 'Mario Benedetti',
		imageUrl: 'https://1.bp.blogspot.com/-nG-xV5VxpvU/V-mGC6OauKI/AAAAAAAABys/uuUx8OsR2ZwhM1ue0WMMppyueEafFrL6gCLcB/s1600/2.jpg',
		genres: [
			'drama',
		],
		description: 'Ex sit sit velit velit magna culpa. Pariatur incididunt aliquip tempor minim nulla.'
	},
	{
		id: '5c703578d99400112f1cfe74',
		title: 'Ready Player One',
		author: 'Ernest Cline',
		imageUrl: 'https://imagessl9.casadellibro.com/a/l/t0/79/9788466649179.jpg',
		genres: [
			'science-fiction',
			'action'
		],
		description: 'Cupidatat pariatur sit proident pariatur dolore in ex elit dolor aliquip officia .'
	},
	{
		id: '5c70357228a48fc499887229',
		title: 'Instrumental',
		author: 'James Rhodes',
		imageUrl: 'https://farmacon.files.wordpress.com/2016/01/james-rhodes-instrumental-portada.jpg',
		genres: [
			'biography'
		],
		description: 'Veniam labore anim Lorem duis nulla nulla ea commodo culpa sint minim proident sunt. Minim mollit ut.'
	},
	{
		id: '5c70357444a48fc499887229',
		title: 'La odisea',
		author: 'Homero',
		imageUrl: 'https://images-na.ssl-images-amazon.com/images/I/51vxVKD3zaL._SX319_BO1,204,203,200_.jpg',
		genres: [
			'myth'
		],
		description: 'Veniam labore anim Lorem duis nulla nulla ea commodo culpa sint minim proident sunt.'
	},
	{
		id: '5c70357808a48fc499887229',
		title: 'El fin de la Infancia',
		author: 'Arthur C. Clarke',
		imageUrl: 'https://cloud10.todocoleccion.online/libros-segunda-mano-ciencia-ficcion-fantasia/tc/2014/06/16/21/43852485.jpg',
		genres: [
			'myth',
			'science-fiction'
		],
		description: 'Veniam labore anim Lorem duis nulla nulla ea commodo culpa sint minim proident sunt est minim mollit ut.'
	}
];
