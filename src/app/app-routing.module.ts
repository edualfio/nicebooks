import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Global templates
import { DashboardComponent } from './components/dashboard/dashboard.component';

// Books components
import { BookListComponent } from './components/books/list/list.component';
import { BookAddComponent } from './components/books/add/add.component';
import { BookEditComponent } from './components/books/edit/edit.component';

// Genres components
import { GenreListComponent } from './components/genres/list/list.component';
import { GenreAddComponent } from './components/genres/add/add.component';
import { GenreEditComponent } from './components/genres/edit/edit.component';

const routes: Routes = [
	{
		path: '',
		component: DashboardComponent
	}, {
		path: 'books',
		component: BookListComponent
	}, {
		path: 'books/add',
		component: BookAddComponent
	}, {
		path: 'books/edit/:id',
		component: BookEditComponent
	}, {
		path: 'genres',
		component: GenreListComponent
	}, {
		path: 'genres/add',
		component: GenreAddComponent
	}, {
		path: 'genres/edit/:id',
		component: GenreEditComponent
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { useHash: true })],
	exports: [RouterModule]
})
export class AppRoutingModule { }
