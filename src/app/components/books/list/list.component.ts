import { Component, OnInit } from '@angular/core';
import { Book } from 'app/shared/models/book.model';
import { BooksService } from 'app/shared/services/books.service';
import swal from 'sweetalert2';

@Component({
	selector: 'app-book-list',
	template: `
	<h1>Book's collection</h1>
	<hr>
	<a class="btn btn-primary" [routerLink]="'/books/add/'">Add new book</a>
	<hr>
	<span class="loader mdi mdi-36px mdi-spin mdi-loading" *ngIf="!isLoaded"></span>
	<table class="table table-sm" *ngIf="isLoaded">
	    <thead class="thead-light">
	        <tr>
	            <th>Title</th>
	            <th>Author</th>
	            <th>Genres</th>
	            <th></th>
	        </tr>
	    </thead>
	    <tbody>
	        <tr *ngFor="let book of books;">
	            <td>{{ book.title }}</td>
	            <td>{{ book.author }}</td>
	            <td>{{ book.genres }}</td>
	            <td class="text-right">
	                <div class="btn-group" role="group">
	                    <button type="button" class="btn btn-sm btn-primary" [routerLink]="['/books/edit/', book.id]">Edit</button>
	                    <button type="button" class="btn btn-sm btn-secondary" (click)="deleteThis(book.id)">Delete</button>
	                </div>
	            </td>
	        </tr>

	    </tbody>
	</table>
  	`
})
export class BookListComponent implements OnInit {
	public books: Array<Book>;
	public isLoaded: boolean;

	constructor(
		private bookservice: BooksService
	) {
		this.isLoaded = false;
		this.retrieveBooks();
	}

	ngOnInit() { }

	retrieveBooks() {
		setTimeout(() => {
			this.books = this.bookservice.getBooks();
			this.isLoaded = true;
		}, 400);
	}

	deleteThis(id) {
		swal({
			type: 'warning',
			title: 'Delete this books. Are you sure?',
			allowOutsideClick: false,
			showCancelButton: true,
			cancelButtonText: 'Cancel',
			confirmButtonClass: 'btn-warning',
			confirmButtonText: 'Delete',
		}).then((isConfirm) => {
			if (isConfirm.value) {
				this.bookservice.deleteBook(id);
			} else {

			}
		});
	}

}
