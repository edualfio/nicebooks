import { TestBed, async } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

// User Interface module
import { UiModule } from './ui/ui.module';

// Global templates
import { DashboardComponent } from './components/dashboard/dashboard.component';

// Books
import { BookListComponent } from './components/books/list/list.component';
import { BookAddComponent } from './components/books/add/add.component';
import { BookEditComponent } from './components/books/edit/edit.component';

// Genres
import { GenreListComponent } from './components/genres/list/list.component';
import { GenreAddComponent } from './components/genres/add/add.component';
import { GenreEditComponent } from './components/genres/edit/edit.component';


describe('AppComponent', () => {
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				UiModule,
				FormsModule
			],
			declarations: [
				AppComponent,
				DashboardComponent,
				BookListComponent,
				BookAddComponent,
				BookEditComponent,
				GenreListComponent,
				GenreAddComponent,
				GenreEditComponent
			],
			providers: [
				{ provide: APP_BASE_HREF, useValue: '/' }
			]
		}).compileComponents();
	}));

	it('should create the app', () => {
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app).toBeTruthy();
	});

	it(`should have as title 'niceBooks'`, () => {
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app.title).toEqual('niceBooks');
	});

});
