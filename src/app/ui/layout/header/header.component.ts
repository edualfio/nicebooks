import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-header',
	template: `
	<nav class="navbar navbar-expand-md navbar-dark bg-primary">
	    <a class="navbar-brand" [routerLink]="'/'">niceBooks!</a>
	    <button class="navbar-toggler hidden-sm-up" type="button"
		(click)="isNavbarCollapsed = !isNavbarCollapsed" data-target="#navbarHeader">
	        <span class="navbar-toggler-icon"></span>
	    </button>

	    <div [ngbCollapse]="isNavbarCollapsed" class="collapse navbar-collapse" id="navbarHeader">
	        <ul class="nav navbar-nav navbar-right">
	            <li class="nav-item dropdown" ngbDropdown>
	                <a class="nav-link dropdown-toggle" id="books_drop" ngbDropdownToggle>Books</a>
	                <div class="dropdown-menu" ngbDropdownMenu>
	                    <a class="nav-link" [routerLink]="'/books'">List</a>
	                    <a class="nav-link" [routerLink]="'books/add'">Add new book</a>
	                </div>
	            </li>
	            <li class="nav-item dropdown" ngbDropdown>
	                <a class="nav-link dropdown-toggle" id="categories_drop" ngbDropdownToggle>Genres</a>
	                <div class="dropdown-menu" ngbDropdownMenu>
	                    <a class="nav-link" [routerLink]="'/genres'">List</a>
	                    <a class="nav-link" [routerLink]="'genres/add'">Add new genre</a>
	                </div>
	            </li>
	        </ul>
	    </div>
	</nav>
	`,
	styles: [`
		.navbar-brand {
			font-family: 'Suez One', serif;
			font-size: 30px;
			color: white;
		}
		.navbar-nav .nav-link {
			padding: 0.5rem 1rem;
			cursor: pointer;
		}
	`]
})
export class HeaderComponent implements OnInit {
	public isNavbarCollapsed: boolean;
	public title: string;

	constructor() {
		this.isNavbarCollapsed = true;
		this.title = 'nice books!';
	}

	ngOnInit() { }

}
