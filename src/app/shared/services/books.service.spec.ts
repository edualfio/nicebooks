import { TestBed } from '@angular/core/testing';

import { BooksService } from './books.service';
import { books } from 'app/shared/mocks/books.mock';


describe('BooksService', () => {
	beforeEach(() => TestBed.configureTestingModule({}));

	it('should be created', () => {
		const service: BooksService = TestBed.get(BooksService);
		expect(service).toBeTruthy();
	});

	it('Books are delivered', () => {
		const service: BooksService = TestBed.get(BooksService);
		expect(service.getBooks()).toBe(books);
	});

	it('Certain Book is delivered', () => {
		const service: BooksService = TestBed.get(BooksService);
		expect(service.getBook('5c703578f93a62cdcd3273bd')).toBe(books[0]);
	});

});
