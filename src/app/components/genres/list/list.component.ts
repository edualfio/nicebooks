import { Component, OnInit } from '@angular/core';
import { Genre } from 'app/shared/models/genre.model';
import { GenresService } from 'app/shared/services/genres.service';
import swal from 'sweetalert2';

@Component({
	selector: 'app-genre-list',
	template: `
	<h1>Genre's collection</h1>
	<hr>
	<a class="btn btn-primary" [routerLink]="'/genres/add/'">Add new genre</a>
	<hr>
	<span class="loader mdi mdi-36px mdi-spin mdi-loading" *ngIf="!isLoaded"></span>
	<table class="table table-sm" *ngIf="isLoaded">
	    <thead class="thead-light">
	        <tr>
	            <th>Name</th>
	            <th>Code</th>
	            <th></th>
	        </tr>
	    </thead>
	    <tbody>
	        <tr *ngFor="let genre of genres;">
	            <td>{{ genre.name }}</td>
	            <td>{{ genre.code }}</td>
	            <td class="text-right">
	                <div class="btn-group" role="group">
	                    <button type="button" class="btn btn-sm btn-primary" [routerLink]="['/genres/edit/', genre.id]">Edit</button>
	                    <button type="button" class="btn btn-sm btn-secondary" (click)="deleteThis(genre.id, genre.code)">Delete</button>
	                </div>
	            </td>
	        </tr>

	    </tbody>
	</table>
	`

})
export class GenreListComponent implements OnInit {
	public genres: Array<Genre>;
	public isLoaded: boolean;

	constructor(
		private genreservice: GenresService
	) {
		this.isLoaded = false;
		this.retrieveGenres();
	}

	ngOnInit() {
	}

	retrieveGenres() {
		setTimeout(() => {
			this.genres = this.genreservice.getGenres();
			this.isLoaded = true;
		}, 400);
	}

	deleteThis(id, code) {
		swal({
			type: 'warning',
			title: 'Delete this genre: ' + code + '. Are you sure?',
			allowOutsideClick: false,
			showCancelButton: true,
			cancelButtonText: 'Cancel',
			confirmButtonClass: 'btn-warning',
			confirmButtonText: 'Delete',
		}).then((isConfirm) => {
			if (isConfirm.value) {
				this.genreservice.deleteGenre(id, code);
			} else {

			}
		});
	}

}
