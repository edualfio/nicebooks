import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Genre } from 'app/shared/models/genre.model';
import { GenresService } from 'app/shared/services/genres.service';
import * as functions from 'app/shared/functions/';
import swal from 'sweetalert2';

@Component({
	selector: 'app-genre-add',
	templateUrl: '../common/form-genre.component.html',
	styleUrls: ['../common/form-genre.component.scss']
})
export class GenreAddComponent implements OnInit {
	public genre: Genre;
	public formTitle: string;
	public isLoaded: boolean;
	public page: string;
	public slugCode: boolean;
	public submitText: string;

	constructor(
		private genreservice: GenresService,
		private router: Router
	) {
		this.genre = new Genre('', '', '', '');
		this.formTitle = 'Add new genre';
		this.isLoaded = true;
		this.page = 'add';
		this.slugCode = true;
		this.submitText = 'Add';
	}

	ngOnInit() { }

	onNaming(event: any) {
		if (this.slugCode) {
			this.genre.code = functions.slugMe(event.target.value);
		}
	}

	onSubmit() {
		if (this.genreservice.saveGenre(this.genre) === '200') {
			swal({
				type: 'success',
				title: this.genre.name + ' was added',
				onClose: () => {
					this.router.navigate(['/genres/']);
				}
			});
		} else {
			swal({
				type: 'error',
				title: 'There has been an error, try it later'
			});
		}
	}
}
