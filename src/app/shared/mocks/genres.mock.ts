export const genres = [
	{
		id: '5c72f64b3beec09dc17c74f8',
		code: 'science-fiction',
		name: 'Science Fiction',
		description: 'lorem ipsun dolor sit amet. Consertetum lore maulet'
	},
	{
		id: '5c72f64ba2058fac61ef4b05',
		code: 'drama',
		name: 'Drama',
		description: 'lorem ipsun dolor sit amet. Consertetum lore maulet'
	},
	{
		id: '5c72f64b619370612b567582',
		code: 'action',
		name: 'Action',
		description: 'lorem ipsun dolor sit amet. Consertetum lore maulet'
	},
	{
		id: '5c72f64b28d1f6d42cdf0ff9',
		code: 'biography',
		name: 'Biography',
		description: 'lorem ipsun dolor sit amet. Consertetum lore maulet'
	},
	{
		id: '5c72f64b04d9fcd85a95c385',
		code: 'myth',
		name: 'Mythology',
		description: 'lorem ipsun dolor sit amet. Consertetum lore maulet'
	}
];
