import { TestBed } from '@angular/core/testing';

import { GenresService } from './genres.service';
import { genres } from 'app/shared/mocks/genres.mock';

describe('GenresService', () => {
	beforeEach(() => TestBed.configureTestingModule({}));

	it('should be created', () => {
		const service: GenresService = TestBed.get(GenresService);
		expect(service).toBeTruthy();
	});

	it('Genres are delivered', () => {
		const service: GenresService = TestBed.get(GenresService);
		expect(service.getGenres()).toBe(genres);
	});

	it('Genre is deleted', () => {
		const service: GenresService = TestBed.get(GenresService);
		expect(service.deleteGenre('5c72f64b3beec09dc17c74f8', 'science-fiction')).toBe('200');
	});
});
