import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from 'app/shared/models/book.model';
import { BooksService } from 'app/shared/services/books.service';
import { Genre } from 'app/shared/models/genre.model';
import { GenresService } from 'app/shared/services/genres.service';
import swal from 'sweetalert2';

@Component({
	selector: 'app-book-add',
	templateUrl: '../common/form-book.component.html',
	styleUrls: ['../common/form-book.component.scss']
})
export class BookAddComponent implements OnInit {
	public book: Book;
	public formTitle: string;
	public genres: Array<Genre>;
	public isLoaded: boolean;
	public submitText: string;

	constructor(
		private bookservice: BooksService,
		private genreservice: GenresService,
		private router: Router
	) {
		this.book = new Book('', '', '', '', [], '');
		this.formTitle = 'Add new book';
		this.isLoaded = true;
		this.submitText = 'Add';
		this.retrieveGenres();
	}

	ngOnInit() {
	}

	retrieveGenres() {
		this.genres = this.genreservice.getGenres();
	}

	onSubmit() {
		if (this.bookservice.saveBook(this.book) === '200') {
			swal({
				type: 'success',
				title: this.book.title + ' was added',
				onClose: () => {
					this.router.navigate(['/books/']);
				}
			});
		} else {
			swal({
				type: 'error',
				title: 'There has been an error, try it later'
			});
		}
	}

}
