import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Book } from 'app/shared/models/book.model';
import { books } from 'app/shared/mocks/books.mock';
import * as _ from 'lodash';

@Injectable({
	providedIn: 'root'
})
export class BooksService {

	public responseCode: string;

	constructor() {
		this.responseCode = '200';
	}

	getBook(id): Book {
		let objBook;
		_.filter(books, (o) => {
			if (o.id === id) {
				objBook = o;
			}
		});

		return objBook;
	}

	getBooks(): Array<Book> {
		return books;
	}

	saveBook(body) {
		books.push(body);
		return this.responseCode;
	}

	updateBook(body) {
		return this.responseCode;
	}

	deleteBook(id) {
		books.splice(_.indexOf(books, _.find(books, { id: id })), 1);
		return this.responseCode;
	}

	deleteGenreFromBooks(code) {
		_.map(books, (o) => {
			if (_.includes(o.genres, code)) {
				_.map(o.genres, ((genre, index) => {
					if (genre === code) {
						o.genres.splice(index);
					}
				}));
			}
		});

		return this.responseCode;
	}

}
