import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from 'app/shared/models/book.model';
import { Genre } from 'app/shared/models/genre.model';
import { GenresService } from 'app/shared/services/genres.service';
import * as functions from 'app/shared/functions/';
import swal from 'sweetalert2';

@Component({
	selector: 'app-genre-edit',
	templateUrl: '../common/form-genre.component.html',
	styleUrls: ['../common/form-genre.component.scss']
})
export class GenreEditComponent implements OnInit {
	public books: Array<Book>;
	public formTitle: string;
	public genre: Genre;
	public isLoaded: boolean;
	public page: string;
	public slugCode: boolean;
	public submitText: string;

	constructor(
		private genreservice: GenresService,
		private route: ActivatedRoute,
		private router: Router
	) {
		this.books = [];
		this.formTitle = 'Edit genre';
		this.genre = new Genre('', '', '', '');
		this.isLoaded = false;
		this.page = 'edit';
		this.slugCode = true;
		this.submitText = 'Update';
		this.retrieveGenre(this.route.snapshot.paramMap.get('id'));
		this.retrieveBooksByGenre();
	}

	ngOnInit() { }

	onNaming(event: any) {
		if (this.slugCode && this.page !== 'edit') {
			this.genre.code = functions.slugMe(event.target.value);
		}
	}

	retrieveBooksByGenre() {
		setTimeout(() => {
			this.books = this.genreservice.getBooksByGenre(this.genre.code);
		}, 600);
	}

	retrieveGenre(id) {
		setTimeout(() => {
			this.genre = this.genreservice.getGenre(id);
			this.isLoaded = true;
		}, 400);
	}

	onSubmit() {
		if (this.genreservice.updateGenre(this.genre) === '200') {
			swal({
				type: 'success',
				title: this.genre.name + ' was updated',
				onClose: () => {
					this.router.navigate(['/genres/']);
				}
			});
		} else {
			swal({
				type: 'error',
				title: 'There has been an error, try it later'
			});
		}
	}

}
