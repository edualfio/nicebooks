import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Genre } from 'app/shared/models/genre.model';
import { Book } from 'app/shared/models/book.model';
import { BooksService } from 'app/shared/services/books.service';
import { genres } from 'app/shared/mocks/genres.mock';
import { books } from 'app/shared/mocks/books.mock';
import * as _ from 'lodash';

@Injectable({
	providedIn: 'root'
})
export class GenresService {

	constructor(
		private booksService: BooksService
	) { }

	getGenre(id): Genre {
		let objGenre;
		_.filter(genres, (o) => {
			if (o.id === id) {
				objGenre = o;
			}
		});

		return objGenre;
	}

	getBooksByGenre(genre): Array<Book> {
		const returnBooks = [];
		_.map(books, (o) => {
			if (_.includes(o.genres, genre)) {
				returnBooks.push(o);
			}
		});

		return returnBooks;
	}

	getGenres(): Array<Genre> {
		return genres;
	}

	saveGenre(body) {
		genres.push(body);
		let responseCode;
		return responseCode = '200';
	}

	updateGenre(body) {
		let responseCode;
		return responseCode = '200';
	}

	deleteGenre(id, code) {
		genres.splice(_.indexOf(genres, _.find(genres, { id: id })), 1);

		if (this.booksService.deleteGenreFromBooks(code) === '200') {
			let responseCode;
			return responseCode = '200';
		}

	}

}
