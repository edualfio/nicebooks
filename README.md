# niceBooks!

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.2.

## First Step to run niceBooks!

`npm install` & `npm start`. That command runs the dev server.

Navigate to `http://localhost:4200/`.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Further help

### App's Folder structure

Main folder into src folder is `/app` that contains the `app.module` (also the *component* and the *routing* file) and 3 differents slices of the project:

- **`UI`**: Interface Module. This module is responsible only for managing the main structure of the front. On the one hand the header and on the other the main content. It does not harbor any logic.  
- **`Components`**: Here we define all the elements represented in the SPA. We distinguish three blocks: the main dashboard, book management, and literary genres. Both include a CRUD model: Listing, editing and deletion from the main view, and the possibility of adding a new entity.
- **`Shared`**: All the services, models, functions and data mocks are collected in a single place. There are files of each type per entity.

### Methods


#### List
A representation of the Book Models elements into a table. Two CTA with actions are present: Edit the current book, and Delete this by id (Actually this triggers an alert window to confirm the action).  Data comes from mockups
#### Add
Form that sends to its service the data of the model. In books add form, you can add a image url to rich the content of the presentation in the final page.
#### Edit
The edition share the html markup with add method. Data is shown into files through NG data binding. The persistence of data is not permanent in case of browser reloading. On genre edit page, there are a list of related books.
#### Delete
Deletes an element form the persistence data. In case of removing a genre, that affects books marked with that genre.


### Other considerations

The project is built with the last version of Angular (7.3.2).

All the code follows the best practices proposed by the team of angular for the legibility of the project, as well as the use of Atom Beautifier and a linter of Typescript.

As the calls to the endpoints of the rest API are simulated, I have added false load times.

In the unit tests I added some functions to test certain methods, as well as the integrity of the components used.

I have used Bootstrap 4 as a layout framework, which is responsive.

I use several third-party dependencies: material design icons, lodash, moment and sweetalert2. All installed by npm.

You can find a build on my server [niceBooks!](http://edualfaro.com/nicebooks/)

The project it works on Firefox Chrome (last version of both) and Internet Explorer 9+
