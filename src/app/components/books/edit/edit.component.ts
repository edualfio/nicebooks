import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from 'app/shared/models/book.model';
import { BooksService } from 'app/shared/services/books.service';
import { Genre } from 'app/shared/models/genre.model';
import { GenresService } from 'app/shared/services/genres.service';
import swal from 'sweetalert2';

@Component({
	selector: 'app-book-edit',
	templateUrl: '../common/form-book.component.html',
	styleUrls: ['../common/form-book.component.scss']
})
export class BookEditComponent implements OnInit {
	public book: Book;
	public formTitle: string;
	public genres: Array<Genre>;
	public isLoaded: boolean;
	public submitText: string;

	constructor(
		private bookservice: BooksService,
		private genreservice: GenresService,
		private route: ActivatedRoute,
		private router: Router
	) {
		this.book = new Book('', '', '', '', [], '');
		this.formTitle = 'Edit book';
		this.isLoaded = false;
		this.submitText = 'Update';
		this.retrieveGenres();
		this.retrieveBook(this.route.snapshot.paramMap.get('id'));
	}

	ngOnInit() { }


	retrieveGenres() {
		this.genres = this.genreservice.getGenres();
	}

	retrieveBook(id) {
		setTimeout(() => {
			this.book = this.bookservice.getBook(id);
			this.isLoaded = true;
		}, 1000);
	}

	onSubmit() {
		if (this.bookservice.updateBook(this.book) === '200') {
			swal({
				type: 'success',
				title: this.book.title + ' was updated',
				onClose: () => {
					this.router.navigate(['/books/']);
				}
			});
		} else {
			swal({
				type: 'error',
				title: 'There has been an error, try it later'
			});
		}
	}

}
