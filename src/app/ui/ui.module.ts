import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from '../app-routing.module';

import { LayoutComponent } from './layout/layout.component';
import { HeaderComponent } from './layout/header/header.component';
import { MainComponent } from './layout/main/main.component';

@NgModule({
	declarations: [LayoutComponent, HeaderComponent, MainComponent],
	imports: [
		AppRoutingModule,
		CommonModule,
		NgbModule.forRoot()
	],
	exports: [
		AppRoutingModule,
		LayoutComponent
	]
})
export class UiModule { }
