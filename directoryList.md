|-- proyectos
    |-- .DS_Store
    |-- .editorconfig
    |-- .gitignore
    |-- README.md
    |-- angular.json
    |-- package-lock.json
    |-- package.json
    |-- tsconfig.json
    |-- tslint.json
    |-- e2e
    |   |-- protractor.conf.js
    |   |-- tsconfig.e2e.json
    |   |-- src
    |       |-- app.e2e-spec.ts
    |       |-- app.po.ts
    |-- src
        |-- browserslist
        |-- favicon.ico
        |-- index.html
        |-- karma.conf.js
        |-- main.ts
        |-- polyfills.ts
        |-- styles.scss
        |-- test.ts
        |-- tsconfig.app.json
        |-- tsconfig.spec.json
        |-- tslint.json
        |-- app
        |   |-- app-routing.module.ts
        |   |-- app.component.html
        |   |-- app.component.spec.ts
        |   |-- app.component.ts
        |   |-- app.module.ts
        |   |-- components
        |   |   |-- books
        |   |   |   |-- add
        |   |   |   |   |-- add.component.spec.ts
        |   |   |   |   |-- add.component.ts
        |   |   |   |-- common
        |   |   |   |   |-- form-book.component.html
        |   |   |   |   |-- form-book.component.scss
        |   |   |   |-- edit
        |   |   |   |   |-- edit.component.spec.ts
        |   |   |   |   |-- edit.component.ts
        |   |   |   |-- list
        |   |   |       |-- list.component.spec.ts
        |   |   |       |-- list.component.ts
        |   |   |-- dashboard
        |   |   |   |-- dashboard.component.spec.ts
        |   |   |   |-- dashboard.component.ts
        |   |   |-- genres
        |   |       |-- add
        |   |       |   |-- add.component.spec.ts
        |   |       |   |-- add.component.ts
        |   |       |-- common
        |   |       |   |-- form-genre.component.html
        |   |       |   |-- form-genre.component.scss
        |   |       |-- edit
        |   |       |   |-- edit.component.spec.ts
        |   |       |   |-- edit.component.ts
        |   |       |-- list
        |   |           |-- list.component.spec.ts
        |   |           |-- list.component.ts
        |   |-- shared
        |   |   |-- functions
        |   |   |   |-- index.ts
        |   |   |-- mocks
        |   |   |   |-- books.mock.ts
        |   |   |   |-- genres.mock.ts
        |   |   |-- models
        |   |   |   |-- book.model.ts
        |   |   |   |-- genre.model.ts
        |   |   |-- services
        |   |       |-- books.service.spec.ts
        |   |       |-- books.service.ts
        |   |       |-- genres.service.spec.ts
        |   |       |-- genres.service.ts
        |   |-- ui
        |       |-- ui.module.ts
        |       |-- layout
        |           |-- layout.component.ts
        |           |-- header
        |           |   |-- header.component.ts
        |           |-- main
        |               |-- main.component.ts
        |-- assets
        |   |-- .gitkeep
        |   |-- img
        |       |-- not_found.jpg
        |-- environments
            |-- environment.prod.ts
            |-- environment.ts
