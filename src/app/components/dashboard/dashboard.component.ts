import { Component, OnInit } from '@angular/core';
import { Book } from 'app/shared/models/book.model';
import { BooksService } from 'app/shared/services/books.service';
import { Genre } from 'app/shared/models/genre.model';
import { GenresService } from 'app/shared/services/genres.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
	selector: 'app-dashboard',
	template: `
	<h1>{{ title }}</h1>
	<hr>
	<div class="row">
	    <div class="col-sm-6">
	        <div class="card">
	            <div class="card-header">
	                Last book added
	            </div>
	            <div class="card-body" *ngIf="isLoaded">
					<div class="row">
						<div class="col-sm-4">
							<img src="{{ lastBook.imageUrl }}"	class="img-fluid" />
						</div>
						<div class="col-sm-8">
			                <h5 class="card-title">{{ lastBook.title }}</h5>
			                <h6>By {{ lastBook.author }}</h6>
			                <p class="card-text">{{ lastBook.description }}</p>
			                <a [routerLink]="['/books/edit/', lastBook.id]" class="btn btn-primary">Edit</a>
						</div>
					</div>
	            </div>
	            <div class="card-body" *ngIf="!isLoaded">
	                <span class="loader mdi mdi-36px mdi-spin mdi-loading"></span>
	            </div>
	        </div>
	    </div>
	    <div class="col-sm-6">
	        <div class="card">
	            <div class="card-body">
	                <h5 class="card-title">Quick actions</h5>
	                <div class="btn-group" role="group" aria-label="Basic example">
	                    <a class="btn btn-primary" [routerLink]="'/books/add'">Add new book</a>
	                    <a class="btn btn-info" [routerLink]="'/genres/add'">Add new genre</a>
	                </div>
	            </div>
	        </div>
	        <div class="card">
	            <div class="card-body">
	                <h5 class="card-title">Last login</h5>
	                <p class="card-text">{{ lastLogin }}</p>
	            </div>
	        </div>
	    </div>
	</div>
	`,
	styles: [`
		.card {
			margin-bottom: 20px;
		}
	`]
})
export class DashboardComponent implements OnInit {
	public books: Array<Book>;
	public genres: Array<Genre>;
	public isLoaded: boolean;
	public lastBook: Book;
	public lastLogin: string;
	public title: string;

	constructor(
		private bookservice: BooksService,
		private genreservice: GenresService
	) {
		this.title = 'Welcome to niceBooks!';
		this.isLoaded = false;
		this.lastBook = new Book('', '', '', '', [], '');
		this.lastLogin = moment().format('DD-MM-YYYY');
		this.title = 'Welcome to niceBooks!';
		this.retrieveGenres();
		this.retrieveBooks();
	}

	ngOnInit() { }

	retrieveBooks() {
		setTimeout(() => {
			this.books = this.bookservice.getBooks();
			this.lastBook = _.last(this.books);
			this.isLoaded = true;
		}, 1000);
	}

	retrieveGenres() {
		this.genres = this.genreservice.getGenres();
	}


}
