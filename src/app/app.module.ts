import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// User Interface module
import { UiModule } from './ui/ui.module';

// Global templates
import { DashboardComponent } from './components/dashboard/dashboard.component';

// Books
import { BookListComponent } from './components/books/list/list.component';
import { BookAddComponent } from './components/books/add/add.component';
import { BookEditComponent } from './components/books/edit/edit.component';

// Genres
import { GenreListComponent } from './components/genres/list/list.component';
import { GenreAddComponent } from './components/genres/add/add.component';
import { GenreEditComponent } from './components/genres/edit/edit.component';


// Services
import { BooksService } from './shared/services/books.service';
import { GenresService } from './shared/services/genres.service';

@NgModule({
	declarations: [
		AppComponent,
		DashboardComponent,
		BookListComponent,
		BookAddComponent,
		BookEditComponent,
		GenreListComponent,
		GenreAddComponent,
		GenreEditComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		UiModule,
		FormsModule,
		BrowserAnimationsModule,

	],
	providers: [
		BooksService,
		GenresService

	],
	bootstrap: [AppComponent]
})
export class AppModule { }
