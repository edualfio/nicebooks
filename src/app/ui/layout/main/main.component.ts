import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-main',
	template: `
	<div class="container">
		<div class="content-wrapper">
		  <router-outlet></router-outlet>
		</div>
	</div>
  	`,
	styles: [`
		.content-wrapper {
			padding: 30px 0;
		}
	`]
})
export class MainComponent implements OnInit {

	constructor() { }

	ngOnInit() { }

}
